import {takeEvery, call, put, all} from "redux-saga/effects";
import get from "lodash/get";
import {storage, api } from "services";
import authActions from "../actions/auth";
import config from '../../config'


export function* LoginRequest(action) {

    const {values: {username, password}, cb} = action.payload;
    try {
        const {data} = yield call(api.request.post, '/security/auth_check', `_username=${username}&_password=${password}&_subdomain=${config.Subdomain}`);
        yield call(storage.set, "token", data?.token);
        yield call(storage.set, "lifetime", JSON.stringify(data?.lifetime));
        yield call(storage.set, "expires_at", JSON.stringify(data?.expires_at));
        yield put(authActions.Login.success(data));
        yield call(cb.onSuccess);

    } catch (error) {

        yield put(authActions.Login.failure({
            error
        }));
        yield call(cb.onError, get(error, 'response.data'));

    } finally {

    }
}


export function* LogoutRequest() {
    yield call(storage.remove, "user");
    yield call(storage.remove, "token");
}

export default function* root() {
    yield all([
        takeEvery(authActions.Login.REQUEST, LoginRequest),
        takeEvery(authActions.Logout.REQUEST, LogoutRequest),
    ]);
}
