import {takeEvery, call, put, all} from "redux-saga/effects";
import { api } from "services";
import productsActions from "../actions/products";


export function* getProducts(action) {
    const {page, size } = action.payload;

    try {
        const {data} = yield call(api.request.post, `/variations?size=${size}&page=${page}`);
        yield put(productsActions.getAllProducts.success(data));
    } catch (error) {

        yield put(productsActions.getAllProducts.failure({
            error
        }));
    }
}


export default function* root() {
    yield all([
        takeEvery(productsActions.getAllProducts.TRIGGER, getProducts),
    ]);
}
