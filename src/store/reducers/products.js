import productsActions from "../actions/products";

const initialState = {
    loading: false,
    products: {}

};

export default (state = initialState, action) => {

    switch (action.type) {

        case productsActions.getAllProducts.TRIGGER:
            return {
                ...state,
                loading: true,
            };
        case productsActions.getAllProducts.SUCCESS:
            return {
                ...state,
                loading: false,
                products: action.payload
            };

        case productsActions.getAllProducts.FAILURE:
            return {
                ...state,
                loading: false,
                products: {}
            };

        default:
            return state;

    }

};
