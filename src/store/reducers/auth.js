import { storage } from "services";
import authActions from "../actions/auth";

const token = storage.get('token');
const lifetime = storage.get('lifetime');
const expires_at = storage.get('expires_at');

const initialState = {
    isFetched: true,
    isAuthenticated: !!token,
    token: token ? token : '',
    expires_at: expires_at ? JSON.parse(expires_at) : {},
    lifetime:  lifetime ? JSON.parse(lifetime) : {},
};

export default (state = initialState, action) => {

    switch (action.type) {


        case authActions.Login.SUCCESS:
            return {
                ...state,
                isFetched: true,
                isAuthenticated: true,
                token: action.payload?.token,
                expires_at: action.payload?.expires_at,
                lifetime: action.payload?.lifetime
            };

        case authActions.Logout.REQUEST:
        case authActions.Logout.FAILURE:
        case authActions.Login.FAILURE:
            return {
                ...state,
                isFetched: true,
                isAuthenticated: false,
                token: '',
                lifetime: {},
                expires_at: null
            };

        default:
            return state;

    }

};
