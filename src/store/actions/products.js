import { createRoutine } from "redux-saga-routines";

const getAllProducts = createRoutine("GET|_ALL");

export default {
    getAllProducts
};
