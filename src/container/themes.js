const theme = {
    colors: {
        powderWhite: "#FFFDF9",
        persianGreen: "#06B49A",
        lightBlue: "#AFDBD2",
        onyx: "#36313D"
    },
    fonts: ["sans-serif", "Roboto"],
    fontSizes: {
        small: "1em",
        medium: "2em",
        large: "3em"
    },
    gradient: 'linear-gradient(160deg, rgb(22 93 245) 0%, rgba(165,7,255,1) 130%)'
};

export default theme;
