import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import { useForm } from "react-hook-form";
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Copyright from './Copyright';
import  { useStyles } from './styles';
import { Button, Avatar, Input  } from 'components';
import logo from 'assets/images/logo1.png';
import {useDispatch} from "react-redux";
import authActions from "store/actions/auth";
import {useHistory} from "react-router";



const Login = () =>  {
    const classes = useStyles();
    const { register, handleSubmit } = useForm();
    const dispatch = useDispatch();
    const history = useHistory();


    const onSubmit = (data) => {

        let payload = {
            cb: {
                onSuccess: ()=>{
                    setTimeout(()=> {
                        history.push('/');
                    })
                },
                onError: (error)=> {

                }
            },
            values: {
                username: data?.username,
                password: data?.password,
                subdomain: 'face'
            }
        }
        dispatch(authActions.Login.request(payload));
    };


    return (
        <Grid container component="main" className={classes.root}>
            <CssBaseline />
            <Grid item xs={false} sm={4} md={7} className={classes.image} />
            <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
                <div className={classes.paper}>
                    <Avatar logo={logo} className={classes.avatar}/>
                    <Typography component="h1" variant="h5">
                        Login
                    </Typography>
                    <form className={classes.form} onSubmit={handleSubmit(onSubmit)}>
                        <Input
                            variant="outlined"
                            margin="normal"
                            fullWidth
                            id="Username"
                            name="Username"
                            autoComplete="Username"
                            autoFocus
                            label="Username"
                            {...register("username")}
                            required />
                        <Input
                            variant="outlined"
                            margin="normal"
                            fullWidth
                            id="password"
                            name="password"
                            autoComplete="password"
                            type='password'
                            autoFocus
                            label="Password"
                            {...register("password")}
                            required
                        />
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                        >
                            Sign In
                        </Button>

                        <Box mt={5}>
                            <Copyright />
                        </Box>
                    </form>
                </div>
            </Grid>
        </Grid>
    );
}

export default Login;
