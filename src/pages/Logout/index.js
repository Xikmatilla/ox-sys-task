import React, { useEffect } from 'react';
import {useDispatch} from "react-redux";
import authActions from "store/actions/auth";

const Index = () => {
    const dispatch = useDispatch();


    useEffect(()=> {
        dispatch(authActions.Logout.request())
    }, []);


    return (
        <div>

        </div>
    );
};

export default Index;
