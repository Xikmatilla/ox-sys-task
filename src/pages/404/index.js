import React from 'react';
import './style.scss';

const Index = () => {
    return (
        <div style={{marginTop: '150px'}}>
            <div className="FourOhFour">
                <div className="code">404</div>
                <img className="logo" src="https://ox-sys.com/static/media/logo.5b7d583a.svg" alt="ox-sys.com"/>
            </div>
        </div>
    );
};

export default Index;
