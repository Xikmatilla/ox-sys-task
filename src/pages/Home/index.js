import React, {useEffect, useState, useMemo} from 'react';
import {useSelector, useDispatch} from "react-redux";
import productsActions from "store/actions/products";
import {SearchComponent, TableComponent} from "components";
import TableRow from "@material-ui/core/TableRow";
import {TableFooter} from "@material-ui/core";
import get from "lodash/get";
import Pagination from "@material-ui/lab/Pagination";

import styled from "styled-components";
import {useDebounce} from "hooks";


const Index = () => {

    const dispatch = useDispatch();
    const {loading, products} = useSelector(({products}) => ({
        loading: products.loading,
        products: products.products
    }));


    const [page, setPage] = useState(1);
    const size = 18;
    const [search, setSearch] = useState('');
    const valueDebounce = useDebounce(search, 300);
    const maxModel = useMemo(() => (Math.ceil(get(products, 'total_count', 0) / 18)), [products]);


    useEffect(() => {
        dispatch(productsActions.getAllProducts.trigger({page, size}));

    }, [page]);



    const memoProducts = useMemo(() => {
        if (valueDebounce.length > 0) {
            return get(products, 'items', []).filter(item => item.name.toLowerCase().match(valueDebounce.toLowerCase()))
        } else {
            return get(products, 'items', [])
        }
    }, [valueDebounce, products]);


    return (
        <>

            <div className="search_section">
                <SearchComponent
                    placeholder={'Поиск'}
                    value={search} onChange={event => setSearch(event.target.value)}/>
            </div>

            <TableComponent
                {...{loading, products: memoProducts}}
            />

            <TableFooterT>
                <TableRow>
                    <div
                        style={{width: '100%'}}
                        className="w-100 d-flex justify-content-center align-content-center mt-3 mb-3">
                        <Pagination
                            count={maxModel}
                            size="large"
                            page={page}
                            onChange={(event, value) => {
                                setPage(value);
                            }}
                        />
                    </div>
                </TableRow>
            </TableFooterT>

        </>
    );
};


const TableFooterT = styled(TableFooter)`
  display: flex;
  justify-content: center;
  background: #ffff;

`

export default Index;
