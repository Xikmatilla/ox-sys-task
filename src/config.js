const {REACT_APP_API_ROOT} = process.env;

const config = {
    API_ROOT: REACT_APP_API_ROOT,
    Subdomain: 'face'
};

export default config;
