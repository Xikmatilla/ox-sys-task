import React, {Suspense} from "react";
import ReactDOM from "react-dom";
import {Provider} from "react-redux";
import {ThemeProvider} from "styled-components";
import {BrowserRouter as Router} from "react-router-dom";
import {api} from "services";
import {configure as configureStore} from "store";
import Routes from "./routes";
import "assets/styles/index.scss";
import theme from 'container/themes';


const store = configureStore();
store.subscribe(() => {
    api.subscribe(store);
});

const render = (Component) => {
    ReactDOM.render(
        <Provider {...{store}}>

            <Router>
                <Suspense fallback="">
                    <ThemeProvider theme={theme}>
                        <Component {...{store}} />
                    </ThemeProvider>
                </Suspense>
            </Router>
        </Provider>,
        document.getElementById("root")
    );
};


render(Routes);

if (module.hot) {
    module.hot.accept("./routes", () => {
        const NextApp = require("./routes").default;
        render(NextApp);
    });
}
