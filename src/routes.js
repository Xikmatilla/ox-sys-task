import React, {lazy, Suspense} from "react";
import {Router, Route, Switch} from "react-router-dom";
import {history} from "store";
import {Redirect} from "react-router";

import {Layout} from "components";
import {Spinner} from "components";
import App from "./App";

const Home = lazy(() => import("./pages/Home"));
const Login = lazy(() => import("./pages/Login/index"));
const Logout = lazy(() => import("./pages/Logout"));
const NoFound = lazy(() => import("./pages/404"));

const routes = [
    {path: "/", exact: true, component: Home},
    {path: "/logout", exact: true, component: Logout, permission: true}
];


export default () => (
    <Router {...{history}}>
        <App>
            {({isFetched, isAuthenticated}) => (
                isFetched && (
                    isAuthenticated ? (
                        <Layout>
                            <Suspense fallback={<Spinner position={"full"}/>}>
                                <Switch>
                                    {routes.map((route, key) => {
                                            return (
                                                <Route
                                                    key={key}
                                                    path={route.path}
                                                    component={route.component}
                                                    exact
                                                />
                                            )
                                    })
                                    }
                                    <Route path='*' exact={true} component={NoFound}/>
                                </Switch>
                            </Suspense>
                        </Layout>
                    ) : (
                        <Suspense fallback={<Spinner position={"full"}/>}>
                            <Switch>
                                <Route path="/" component={Login} exact/>
                                <Redirect from="*" to="/"/>
                            </Switch>
                        </Suspense>
                    )
                )
            )}
        </App>
    </Router>
);
