import React, { forwardRef } from 'react';
import Avatar from '@material-ui/core/Avatar';
import cx from "classnames";

const AvatarComponent = (props, ref) => {

    const {url, className } = props
    const classNames = cx(
        '',
        className,
    );
    return (
        <Avatar ref={ref} size={2} src={url}  alt="Avatar" className={classNames} />
    );
};

export default forwardRef(AvatarComponent);
