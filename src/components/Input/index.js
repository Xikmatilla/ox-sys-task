import React, { forwardRef} from 'react';
import TextField from '@material-ui/core/TextField';

const Input  = (props, ref) => {

    return (
        <>
            <TextField ref={ref} {...props}/>
        </>
    );
};

export default forwardRef(Input);
