import React, { memo} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import get from "lodash/get";
import AvatarComponent from "../Avatar";
import defaultImage from 'assets/images/default.jpg'
import {CopyToClipboard} from 'react-copy-to-clipboard';
import CircularProgress from '@material-ui/core/CircularProgress';


const useStyles = makeStyles((theme)=> ({
    root: {
        width: '100%',
    },
    container: {
        maxHeight: 750,
        minHeight: 750
    },
    spinner: {
        display: 'flex',
        height: 50,
        position: "absolute",
        top: '25%',
        left: '55%',
        transform: 'transform: translate(-50%, -50%)'
    },
}));

const columns = [
    {id: 'photo', label: 'Фото', minWidth: 20},
    {id: 'name', label: 'Имя', minWidth: 120},
    {
        id: 'sellPrice',
        label: 'Цена продажи',
        minWidth: 120
    },
    {
        id: 'code',
        label: 'Бренд-код',
        minWidth: 170
    },
];


const TableComponent = ({loading, products}) => {

    const classes = useStyles();

    return (
        <Paper className={classes.root}>
            <TableContainer className={classes.container}>
                <Table stickyHeader aria-label="sticky table">
                    <TableHead>
                        <TableRow>
                            {columns.map((column) => (
                                <TableCell
                                    key={column.id}
                                    align={column.align}
                                    style={{minWidth: column.minWidth}}
                                >
                                    {column.label}
                                </TableCell>
                            ))}
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {loading ? (
                            <div style={{height: 750}}>
                                <div className={classes.spinner}>
                                    <CircularProgress />
                                </div>
                            </div>
                            ) : (
                                <>
                                    {products.length > 0 ? products.map((item) => {
                                        return (
                                            <TableRow key={item.id} hover role="checkbox" tabIndex={-1}>
                                                <TableCell>
                                                    <AvatarComponent url={get(item, 'images[0].urls.100x_', defaultImage)}/>
                                                </TableCell>
                                                <TableCell>
                                                    {item.name}
                                                </TableCell>
                                                <TableCell>
                                                    {get(item, 'stocks[0].sellPrice.USD', 0)}    &nbsp; USD
                                                </TableCell>
                                                <TableCell>
                                                    <CopyToClipboard text={get(item, 'barcode', null)}
                                                                     onCopy={() => get(item, 'barcode', null)}>
                                                        <span> {get(item, 'barcode', null)}</span>
                                                    </CopyToClipboard>
                                                </TableCell>
                                            </TableRow>
                                        );
                                    }) : null}
                                </>
                        )}

                    </TableBody>


                </Table>
            </TableContainer>

        </Paper>
    );
}


const propsAreEqual = (prevProps, nextProps) => {
    return prevProps.loading === nextProps.loading
        && prevProps.products === nextProps.products;
}


export default memo(TableComponent, propsAreEqual);
