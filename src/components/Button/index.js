import React, { memo} from 'react';
import Button from "@material-ui/core/Button";
import cx from 'classnames';
import PropTypes from 'prop-types';

const ButtonComponent = ({className, children,  ...props}) => {

    const classNames = cx(
        '',
        className,
    );
    return (
        <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            {...props}
            className={classNames}
        >
            {children}
        </Button>
    );
};


ButtonComponent.propTypes = {
    children: PropTypes.string,
    className: PropTypes.string
};
ButtonComponent.defaultProps = {
    children: "",
    className: ''
};


const propsAreEqual = (prevProps, nextProps) =>{
    return prevProps.children === nextProps.children
        && prevProps.className === nextProps.className;
}

export default memo(ButtonComponent, propsAreEqual);
