import React, {memo} from 'react';
import styled from "styled-components";
import Input from '@material-ui/core/Input';
import PropTypes from 'prop-types';

const SearchComponent = ({value, placeholder, onChange, ...props}) => {
    return (
        <SearchInput
            value={value}
            placeholder={placeholder}
            onChange={onChange}
            {...props}/>
    );
};

const SearchInput = styled(Input)`
  border: .5px solid #828286;
  border-radius: 5px;
  padding: 5px 10px;
  font-size: 16px;

  &:focus {
    border-bottom: .5px solid #828286;
  }
`


SearchComponent.propTypes = {
    value: PropTypes.string,
    placeholder: PropTypes.string,
    onChange: PropTypes.func,

};
SearchComponent.defaultProps = {
    value: "",
    placeholder: '',
    onChange: () => {
    }
};


const propsAreEqual = (prevProps, nextProps) => {
    return prevProps.value === nextProps.value
        && prevProps.placeholder === nextProps.placeholder;
}

export default memo(SearchComponent, propsAreEqual);
