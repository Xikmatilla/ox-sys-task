import React, { memo } from 'react';
import clsx from "clsx";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import Typography from "@material-ui/core/Typography";
import Badge from "@material-ui/core/Badge";
import AppBar from "@material-ui/core/AppBar";
import {useStyles} from "./useStyles";
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import styled from "styled-components";
import { useHistory } from "react-router";

const Header = ({open, handleDrawerOpen}) => {
    const classes = useStyles();
    const [anchorEl, setAnchorEl] = React.useState(null);
    const history = useHistory();

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        history.push('/logout');
        setAnchorEl(null);

    };
    return (
        <AppHeader position="absolute" className={clsx(classes.appBar, open && classes.appBarShift)}>
            <Toolbar className={classes.toolbar}>
                <IconButton
                    edge="start"
                    color="inherit"
                    aria-label="open drawer"
                    className={clsx(classes.menuButton, open && classes.menuButtonHidden)}
                    onClick={handleDrawerOpen}
                >
                    <MenuIcon />
                </IconButton>
                <Typography component="h1" variant="h6" color="inherit" noWrap className={classes.title}>
                    OX System.OOO «CREATIVE PEOPLE»
                </Typography>
                <IconButton color="inherit">
                    <Badge onClick={handleClick} color="secondary">
                        <AccountCircleIcon />
                    </Badge>
                </IconButton>

                <Menu
                    id="simple-menu"
                    anchorEl={anchorEl}
                    keepMounted
                    open={Boolean(anchorEl)}
                    onClose={handleClose}
                >
                    <MenuItem onClick={handleClose}>Logout</MenuItem>
                </Menu>
            </Toolbar>
        </AppHeader>
    );
};


const AppHeader = styled(AppBar)`
  && {
    background: ${props => props.theme.gradient};
  }
    
`

const propsAreEqual = (prevProps, nextProps) =>{
    return prevProps.open === nextProps.open
        && prevProps.handleDrawerOpen === nextProps.handleDrawerOpen;
}

export default memo(Header,propsAreEqual);
