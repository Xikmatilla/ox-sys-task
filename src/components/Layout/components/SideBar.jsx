import React, { memo } from 'react';
import { useLocation } from "react-router";
import clsx from 'clsx';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import DashboardIcon from '@material-ui/icons/Dashboard';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import PeopleIcon from '@material-ui/icons/People';
import LayersIcon from '@material-ui/icons/Layers';
import { useStyles } from './useStyles'
import { useSelector } from 'react-redux';
import get from 'lodash/get';
import ListItemLink from "./ListItemLink";

const SideBar = ({open, handleDrawerClose}) =>  {
    const classes = useStyles();
    const auth = useSelector(({auth})=> auth);
    const location = useLocation();

    const menuItem = [
        {id: 1, title: 'Главная', path: "/", icon: <DashboardIcon />},
        {id: 2, title: 'Orders', path: '/orders', icon: <ShoppingCartIcon />},
        {id: 3, title: 'Reports', path: '/reports', icon: <PeopleIcon />},
        {id: 4, title: 'Integrations', path: '/Integrations', icon: <LayersIcon />},
    ]

    console.log(location)
    return (
            <Drawer
                variant="permanent"
                classes={{
                    paper: clsx(classes.drawerPaper,'side_bar', !open && classes.drawerPaperClose),
                }}
                open={open}
            >
                <div className={classes.toolbarIcon}>
                    <div style={{ paddingLeft: '5px', fontSize: '1rem'}}>
                        {auth &&  get(auth, 'expires_at', '')}
                    </div>
                    <IconButton onClick={handleDrawerClose}>
                        <ChevronLeftIcon />
                    </IconButton>
                </div>
                <Divider />
                <List style={{ zIndex: 3}}>
                    {menuItem.map(menu => (
                        <ListItemLink
                            isActive={get(location, 'pathname', '') === menu.path}
                            key={menu.id}
                            button
                            icon={menu.icon}
                            to={menu.path}
                            primary={menu.title}
                        />
                    ))}

                </List>
                <Divider/>
            </Drawer>
    );
}


const propsAreEqual = (prevProps, nextProps) =>{
    return prevProps.open === nextProps.open
        && prevProps.handleDrawerClose === nextProps.handleDrawerClose;
}


export default memo(SideBar, propsAreEqual);
