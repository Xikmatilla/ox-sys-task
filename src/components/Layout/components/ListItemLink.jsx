import React from 'react';
import { Link  } from 'react-router-dom';
import {ListItem, ListItemIcon, ListItemText} from "@material-ui/core";
import styled  from "styled-components";

const ListItemLink = (props) => {
    const { icon, primary, to, isActive } = props;

    const CustomLink = React.useMemo(
        () =>
            React.forwardRef((linkProps, ref) => (
                <Link className={`${isActive ? 'is-active' : ''}`} ref={ref} to={to} {...linkProps} />
            )),
        [to],
    );

    return (
        <li>
            <List button component={CustomLink}>
                <ListItemIcon>{icon}</ListItemIcon>
                <ListItemText primary={primary} />
            </List>
        </li>
    );
}

const List = styled(ListItem)`
    &&{
      
      &.is-active{
        background: #5762B2;
      }
    }


`

export default ListItemLink;
