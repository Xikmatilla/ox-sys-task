import React from 'react';
import "./style.scss";
import {ErrorBoundary} from "../index";
import Container from "@material-ui/core/Container";
import {useStyles} from './components/useStyles'
import CssBaseline from "@material-ui/core/CssBaseline";

import Header from './components/Header'
import SideBar from "./components/SideBar";

const Layout = (props) => {
    const {children} = props;
    const classes = useStyles();
    const [open, setOpen] = React.useState(true);


    const handleDrawerOpen = () => {
        setOpen(true);
    };
    const handleDrawerClose = () => {
        setOpen(false);
    };
    return (
        <div className={classes.root}>
            <CssBaseline />
            <Header {...{open,handleDrawerOpen}}/>
            <SideBar {...{open, handleDrawerClose }}/>
            <main className={classes.content}>
                <div className={classes.appBarSpacer}/>
                <Container className={classes.container}>
                    <ErrorBoundary>
                        {children}
                    </ErrorBoundary>
                </Container>
            </main>
        </div>
    );
};

export default Layout;
