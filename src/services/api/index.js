import axios from "axios";
import get from "lodash/get";
import config from "config";
import storage from '../storage';

const request = axios.create({
	baseURL: config.API_ROOT
});

request.defaults.params = {};
request.defaults.headers.common['Content-Type'] = "application/x-www-form-urlencoded"

let token = storage.get('token');
const subscribe = store => {
	let state = store.getState();
	if(state.auth.token) token = get(state, 'auth.token');
	if(token){
		request.defaults.headers.common['Authorization'] = `Bearer ${token}`;
	}
};


export default {
	request,
	subscribe
};
