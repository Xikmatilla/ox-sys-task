import React, {Component} from 'react';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {withRouter} from "react-router";

class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      error: null
    }
  }



  render() {

    const { children, auth } = this.props;
    const { error } = this.state;
    if (error) {
      return (
          <div className="error-page error-page__sentry">
            <div className="error-ico"/>
            <div className="error-text">Something went wrong !!!</div>
            <span className="error-btn mx-btn btn-solid info">Report feedback</span>
          </div>
      );
    }

    return children(auth);
  }
}

const mapStateToProps = state => ({
  auth: state.auth
});


const mapDispatchToProps = (dispatch) => bindActionCreators(
    {},
    dispatch
);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
